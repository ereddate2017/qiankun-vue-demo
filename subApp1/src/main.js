import "./public-path";
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import routes from "./router";
import store from "./store";
import animated from "animate.css";
import iconfont from "./assets/fonts/iconfont.css";
import VueI18n from "vue-i18n";

Vue.config.productionTip = false;
//引入nprogress
import NProgress from "nprogress"; // 进度条
import "nprogress/nprogress.css"; //这个样式必须引入
// 简单配置
NProgress.inc(0.5);
NProgress.configure({ easing: "ease", speed: 500, showSpinner: false });

Vue.use(animated).use(iconfont).use(VueI18n);

const i18n = new VueI18n({
  locale: "zh-cn",
  messages: {
    "en-us": require("./assets/lang/en-us.js"),
    "zh-cn": require("./assets/lang/zh-cn.js"),
  },
});

let router = null;
let instance = null;
function render(props = {}) {
  const { container } = props;
  router = new VueRouter({
    base: window.__POWERED_BY_QIANKUN__ ? "/customerService/" : "/",
    mode: "history",
    routes,
  });

  router.beforeEach((to, from, next) => {
    console.log("[app21 beforeEach]", to, from);
    NProgress.start();
    document.title = to.meta.title || document.title;
    next();
  });

  router.afterEach((to, from) => {
    console.log("[app21 afterEach]", to, from);
    NProgress.done();
  });

  instance = new Vue({
    router,
    store,
    i18n,
    render: (h) => h(App),
  }).$mount(container ? container.querySelector("#app") : "#app");
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {
  console.log("[vue] vue app bootstraped");
}
export async function mount(props) {
  console.log("[vue] props from main framework", props);

  props.onGlobalStateChange((state, prev) => {
    // state: 变更后的状态; prev 变更前的状态
    console.log(state, prev);
  });

  render(props);
}
export async function unmount() {
  instance.$destroy();
  instance.$el.innerHTML = "";
  instance = null;
  router = null;
}
