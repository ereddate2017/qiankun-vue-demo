export const loginMutations = {
  setToken(state, value) {
    state.token = value;
    localStorage.setItem("Token", value);
  },
  getToken(state) {
    return state.token || localStorage.getItem("Token");
  },
  hasToken(state) {
    let token = state.token || localStorage.getItem("Token");
    return token ? true : false;
  },
};
