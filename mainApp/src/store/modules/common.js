import { config } from "../../utils/config.js";
export const commonActions = {
  async postData(store, api, data) {
    let result = await fetch(config.apiBaseDomain + api, {
      method: "POST",
      mode: "cors",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    return result.json();
  },
};

export const commonMutations = {
  setIsMicroApp(state, v) {
    state.isMicroApp = v;
  },
};
