import { config } from "../../utils/config.js";
export const homeActions = {
  async getHomePageData(store, json) {
    return await store.dispatch("postData", "/getRecommendDiaryList", json);
  },
};
