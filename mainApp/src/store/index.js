import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import { commonActions, commonMutations } from "./modules/common.js";
import { homeActions } from "./modules/home.js";
import { loginMutations } from "./modules/login.js";

const store = new Vuex.Store({
  state: {
    isMicroApp: false,
    Token: null,
  },
  mutations: {
    ...loginMutations,
    ...commonMutations,
  },
  actions: {
    ...commonActions,
    ...homeActions,
  },
});

export default store;
