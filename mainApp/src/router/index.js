import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
export const routes = [
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName:'home' */ "../views/home.vue"),
    meta: {
      title: "Home",
      icon: "icon-home",
      back: false,
    },
  },
  {
    path: "/recommend",
    name: "recommend",
    component: () =>
      import(/* webpackChunkName:'user' */ "../views/recommend.vue"),
    meta: {
      title: "Recommend",
      icon: "icon-search",
      back: false,
    },
    children: [
      {
        path: "/search",
        name: "search",
        component: () =>
          import(/* webpackChunkName:'search' */ "../views/search.vue"),
        meta: {
          title: "Search",
          back: true,
        },
      },
    ],
  },
  {
    path: "/add",
    name: "add",
    component: () => import(/* webpackChunkName:'user' */ "../views/add.vue"),
    meta: {
      title: "Add",
      icon: "icon-add-bold",
      back: false,
    },
  },
  {
    path: "/message",
    name: "message",
    component: () =>
      import(/* webpackChunkName:'user' */ "../views/message.vue"),
    meta: {
      title: "Message",
      icon: "icon-comment",
      back: false,
    },
  },
  {
    path: "/user",
    name: "user",
    component: () => import(/* webpackChunkName:'user' */ "../views/user.vue"),
    meta: {
      title: "User",
      icon: "icon-user",
      back: false,
    },
  },
];

export const router = new VueRouter({
  mode: "history",
  routes,
});
