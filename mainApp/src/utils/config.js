export const config = {
  apiBaseDomain:
    "https://www.fastmock.site/mock/2495ee95fc429a6d0c761ef4e9f5a696/test",
};

import {
  registerMicroApps,
  start,
  initGlobalState,
  MicroAppStateActions,
} from "qiankun";
import { routes } from "../router/index";

function find(to, arr) {
  let isMicroApp = true;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].name === to.name) {
      isMicroApp = false;
      break;
    }
    if (arr[i].children) {
      isMicroApp = find(to, arr[i].children);
    }
  }
  return isMicroApp;
}

export const microApp = {
  config() {
    registerMicroApps([
      {
        name: "customer service",
        entry: "//192.168.246.115:8081",
        container: "#container",
        activeRule: "/customerService",
      },
      {
        name: "login",
        entry: "//192.168.246.115:8082",
        container: "#container",
        activeRule: "/login",
      },
    ]);
    // 初始化 state
    const actions = initGlobalState({});

    actions.onGlobalStateChange((state, prev) => {
      // state: 变更后的状态; prev 变更前的状态
      console.log(state, prev);
    });
    //actions.setGlobalState(state);
    //actions.offGlobalStateChange();

    start();
  },
  hasMicroApp(to) {
    let isMicroApp = find(to, routes);
    return isMicroApp;
  },
};
