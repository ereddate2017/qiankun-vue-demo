import Vue from "vue";
import App from "./App.vue";
import { router, routes } from "./router/index";
import store from "./store/index";
import "./utils/rem.js";
import animated from "animate.css";
import iconfont from "./assets/fonts/iconfont.css";
import VueI18n from "vue-i18n";
import { microApp } from "./utils/config";

//引入nprogress
import NProgress from "nprogress"; // 进度条
import "nprogress/nprogress.css"; //这个样式必须引入
// 简单配置
NProgress.inc(0.5);
NProgress.configure({ easing: "ease", speed: 500, showSpinner: false });

Vue.use(animated).use(iconfont).use(VueI18n);

const i18n = new VueI18n({
  locale: "zh-cn",
  messages: {
    "en-us": require("./assets/lang/en-us.js"),
    "zh-cn": require("./assets/lang/zh-cn.js"),
  },
});

console.log(i18n);

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  console.log("[router beforeEach]", to, from);
  NProgress.start();
  document.title = to.meta.title || document.title;
  let hasToken = store.state.Token || localStorage.getItem("Token");
  !store.state.Token && store.commit("setToken", hasToken);
  let isMicroApp = microApp.hasMicroApp(to);
  store.commit("setIsMicroApp", isMicroApp);
  console.log("[isMicroApp]", isMicroApp);
  console.log("[hasToken]", hasToken);
  if (!hasToken) {
    if (isMicroApp) {
      next();
    } else {
      next({
        path: "/login?source=" + location.href,
      });
    }
  } else if (hasToken) {
    next();
  }
});

router.afterEach((to, from) => {
  console.log("[router afterEach]", to, from);
  NProgress.done();
});

new Vue({
  store,
  router,
  i18n,
  render: (h) => h(App),
}).$mount("#app");

microApp.config();
